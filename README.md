# pybooks

# Getting Started

### Init DataBase

```shell
$ docker-compose -f docker-compose.yml up -d
```

### Export Environment Variables

* `FLASK_APP`: Entry point of your application; should be `app.py`.
* `SQLALCHEMY_DATABASE_URI`: SQLAlchemy connection URI to a SQL database.

##### For Example

```shell
$ export FLASK_APP=app.py
$ export SQLALCHEMY_DATABASE_URI="postgresql://postgres:postgres@localhost:5433/writers_db"
```

### Installation
```shell
$ git clone https://gitlab.com/frostnova/pybooks.git
$ cd pybooks
```

* Create tables and insert test data.

```shell
$ flask init
```
* Running App.

```shell
$ flask run
```
* Move to http://127.0.0.1:5000/writers/9 

    or http://127.0.0.1:5000/writers/3

    or http://127.0.0.1:5000/writers/5
