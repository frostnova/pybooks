import json
import click
import models
from config import PATH_DATA_FILE
from create import create_app, db
from flask.cli import with_appcontext


app = create_app()


def load_data(file: str):
    with open(file) as file:
        data = json.load(file)

    for key in data.keys():
        writers = models.WritersModel(data[key]['id'], data[key]['name'])

        for book in data[key]['books']:
            writers.books.append(models.BooksModel(**book))

        db.session.add(writers)
        db.session.commit()
        db.session.close()


@click.command('init')
@with_appcontext
def init_db_command():
    """Create new tables adn and insert data."""

    click.echo('Initialized the database.')
    db.create_all()

    load_data(PATH_DATA_FILE)
    click.echo('Successfully completed.')


app.cli.add_command(init_db_command)


if __name__ == '__main__':
    app.run(debug=True)
