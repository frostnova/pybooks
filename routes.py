"""Application routes."""
import models
from flask import request, current_app


@current_app.route('/writers/<writers_id>', methods=['GET'])
def get_writers(writers_id):

    writers = models.WritersModel.query.get_or_404(writers_id)

    if request.method == 'GET':
        response = {
            "id": writers.id,
            "name": writers.name,
            "books": writers.books
        }

        return response
