from os import environ, path

basedir = path.abspath(path.dirname(__file__))

PATH_DATA_FILE = "data/data.json"


class Config:
    """Set Flask configuration from environment"""

    # General Config
    FLASK_APP = environ.get("FLASK_APP")

    # Database
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    JSON_AS_ASCII = False
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = True
