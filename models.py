"""Data models."""
from create import db
from dataclasses import dataclass


@dataclass
class WritersModel(db.Model):
    id: int
    name: str
    __tablename__ = 'writers'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    books = db.relationship("BooksModel", backref="writers")

    def __init__(self, id, name):
        self.id = id
        self.name = name

    def __repr__(self):
        return f"<Writer {self.id}, {self.name}>"


@dataclass
class BooksModel(db.Model):
    id: int
    name: str
    __tablename__ = 'books'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    author_id = db.Column(db.Integer, db.ForeignKey('writers.id'))

    def __init__(self, id, name, author_id):
        self.id = id
        self.name = name
        self.author_id = author_id

    def __repr__(self):
        return f"id: {self.id}, name: {self.name}"
